const express = require("express");
const hbs = require("hbs");
const mysql = require("mysql2");
const path = require("path");

const app = express();
const port = 3000;

//Conexion base de datos

const conn = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "Clave_mysql_123",
  database: "clientes",
});

conn.connect((err) => {
  if (err) throw err;
  console.log("conectado");
});

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/assets", express.static(__dirname + "/public"));

// Rutas
app.get("/", (req, res) => {
  let sql = " SELECT * FROM clientes";
  let query = conn.query(sql, (err, results) => {
    if (err) throw err;
    res.render("cliente_vista", {
      results: results,
    });
  });
});



app.post("/agregar", (req, res) => {
  let data = {
    nombre: req.body.nombre,
    apellido: req.body.apellido,
    telefono: req.body.telefono
  };
  let sql = "INSERT INTO clientes SET ?";
  let query = conn.query(sql, data, (err, results) => {
    if (err) throw err;
    res.redirect("/");
  });
});


app.post("/editar", (req, res) => {
  let sql =
    `UPDATE clientes SET nombre = "${req.body.nombre}" , apellido = "${req.body.apellido}" , telefono = "${req.body.telefono}" WHERE id_cliente =  ${req.body.id_cliente};`
  let query = conn.query(sql, (err, results) => {
    if (err) throw err;
    res.redirect("/");
  });

});

app.post("/borrar", (req, res) => {
  let sql =
    `DELETE from clientes WHERE id_cliente = ${req.body.id_cliente}`
  let query = conn.query(sql, (err, results) => {
    // if (err) throw err;
    if (err)
    console.log(err);

  });
  
  res.redirect("/");
});



app.listen(port, () => {
  console.log(`Usando el puerto http://localhost:${port}`);
});
